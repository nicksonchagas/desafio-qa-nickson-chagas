
# language: pt
# encoding: UTF-8

Funcionalidade: Criação de um grupo e inclusão de participantes

Como usuario do aplicativo WhatsApp na plataforma Android
Eu quero criar um grupo e adicionar pessoas 



Esquema do Cenario: Criação de grupo

	Dado que estou com o sistema WhatsApp aberto em meu dispositivo android
	E acesso o menu CONVERSAS 
	E no menu superior direito seleciono a opção CRIAR GRUPO, será apresentado a tela ADICIONAR PARTICIPANTES
	E ao informar os participantes <nome_participantes> e prosseguir clicando na seta inferior esquerda, será exibido o menu ADICIONAR NOME
	Quando eu adicionar o nome <nome_grupo> ao grupo e clicar na opção FINALIZAR
	Então o grupo será criado com sucesso.
	
	Exemplos:
	| nome_grupo | nome_participantes |
	| "DesafioQA" | "Nickson Chagas,Foco"|
	| "DesafioQA_Enfrentando_Boss" | "Nickson Chagas,Frederico Moreira,Samanta Cicilia"|
	
	
Esquema do Cenario: Inclusão de participantes a um grupo existente

	Dado que estou com o sistema WhatsApp aberto em meu dispositivo android
	E acesso o menu CONVERSAS
	E seleciono o grupo <grupo> do qual possuo permissão administrativa 
	E prossigo clicando na opção DADOS DO GRUPO no menu superior direito, será exibido tela com as configurações do mesmo 
	Quando eu clicar na opção ADICIONAR PARTICIPANTES, informar os participantes <nome_participantes> e clicar na opção FINALIZAR
	Então os participantes <nome_participantes> deveram estar adicionados ao grupo.
	
	Exemplos:
	| grupo 	| participantes    	 |
	| "DesafioQA2" | "Nickson Chagas,Foco"|
	| "DesafioQA2_O_Retorno" | "Nickson Chagas,Frederico Moreira,Samanta Cicilia"|