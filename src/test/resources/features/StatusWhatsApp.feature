# language: pt
# encoding: UTF-8

Funcionalidade: Inclusão de Status e Consulta de quantidade de visualizações 

Como usuario do aplicativo WhatsApp na plataforma Android
Eu quero criar meu status e validar o número de visualizações


Esquema do Cenario: Criação de status

	Dado que estou com o sistema WhatsApp aberto em meu dispositivo android
	E acesso o menu STATUS 
	E no menu superior direito seleciono o item indicado por três pontos, será apresentado a tela com detalhes do status
	E ao clicar no icone representado por uma caneta será exibido a tela com um texto de fundo escrito DIGITE SEU STATUS
	Quando digitar o status <status> e clicar na opção FINALIZAR
	Então o status será criado com sucesso.
	
	Exemplos:
	| status | 
	| "Quando lhe disserem que você não consegue, lembre-se que os grandes heróis já ouviram isso e nunca desistiram." |
	| "Se quer ir rápido, vá sozinho. Se quer ir longe, vá em grupo." |
	
	
	
Esquema do Cenario: Validação de número de visualizações

	Dado que estou com o sistema WhatsApp aberto em meu dispositivo android
	E acesso o menu STATUS 
	E no menu superior direito seleciono o item indicado por três pontos, será apresentado a tela com detalhes do status
	Quando eu clicar no icone representado por um olho 
	Então será apresentado uma mensagem informando o número de visualizações.
	
	