package com.desafioQa.checkout;

import java.util.Map;

/**
 * Implementação do preço especial
 * 
 * @author Nickson chagas
 *
 */
public class SpecialPrice implements Comparable<SpecialPrice> {

	private final String item;
	private final int price;
	private final int count;

	public SpecialPrice(String item, int price, int count) {
		this.item = item;
		this.price = price;
		this.count = count;
	}

	public String getItem() {
		return item;
	}

	public int getPrice() {
		return price;
	}

	public int getCount() {
		return count;
	}

	public Integer getPriori() {
		return 10;
	}

	public int compareTo(SpecialPrice that) {
		return this.getPriori().compareTo(that.getPriori());
	}

	/**
	 * Realiza validação do valor incluindo o valor especial
	 * 
	 * @param items
	 * @return itemsPrice
	 */
	public int apply(Map<String, Integer> items) {
		Integer itemsCount = items.get(item);
		int itemsPrice = itemsCount / count * price;
		int itemsLeft = itemsCount - itemsCount / count * count;
		items.put(item, itemsLeft);
		return itemsPrice;
	}
}
