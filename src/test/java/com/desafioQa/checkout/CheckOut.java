package com.desafioQa.checkout;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * Classe contém funções core para utilização 
 * 
 * @author Nickson chagas
 *
 */
public class CheckOut {

	private Map<String, List<SpecialPrice>> rules = Maps.newHashMap();
	private Map<String, Integer> items = Maps.newHashMap();

	public static CheckOut instance(List<SpecialPrice> rules) {
		return new CheckOut(rules);
	}

	private CheckOut(List<SpecialPrice> rules) {
		for (SpecialPrice rule : rules) {
			String item = rule.getItem();
			List<SpecialPrice> itemRules = this.rules.get(item);
			if (itemRules == null) {
				itemRules = Lists.newArrayList();
				this.rules.put(item, itemRules);
			}
			itemRules.add(rule);
		}
	}

	/**
	 * Responsavél por efetuar a leitura e interpretação de numero de itens,
	 * caso o valor sejá nulo será atribuido o valor 0 ao mesmo.
	 * 
	 * @param item
	 */
	public void scan(String item) {
		Integer itemsCount = items.get(item);
		if (itemsCount == null) {
			itemsCount = 0;
		}
		items.put(item, ++itemsCount);
	}

	/**
	 * Realiza validação do valor do item normal
	 * 
	 * @return Valor total do calculo dos itens
	 */
	public int getTotal() {
		int total = 0;
		for (String item : items.keySet()) {
			total += getTotal(item, Maps.newHashMap(items));
		}
		return total;
	}

	/**
	 * Realiza validaçao do valor do item especial
	 * 
	 * @param item
	 * @param items
	 * @return Valor total
	 */
	private int getTotal(String item, Map<String, Integer> items) {
		List<SpecialPrice> itemRules = rules.get(item);
		if (itemRules == null) {
			throw new IllegalArgumentException("Unknown item: " + item);
		}
		Collections.sort(itemRules);
		int total = 0;
		for (SpecialPrice itemRule : itemRules) {
			total += itemRule.apply(items);
		}
		return total;
	}

}
