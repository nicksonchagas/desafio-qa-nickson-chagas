package com.desafioQa.checkout;

/**
 * Controle de variaveis de preço normal
 * 
 * @author Nickson chagas
 *
 */
public class NormalPrice extends SpecialPrice {

	public NormalPrice(String item, int price) {
		super(item, price, 1);
	}

	public Integer getPriori() {
		return 20;
	}

}
