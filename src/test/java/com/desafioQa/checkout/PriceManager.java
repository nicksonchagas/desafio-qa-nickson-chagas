package com.desafioQa.checkout;

import org.apache.commons.lang.StringUtils;
import com.desafioQa.test.CheckOutTest;

/**
 * Classe com principal validação dos valores
 * 
 * @author Nickson chagas
 *
 */
public class PriceManager {

	/**
	 * Responsavél por efetuar leitura e interpretação do valor do valor
	 * correspondente
	 * 
	 * @param goods
	 * @return
	 */
	public int price(String goods) {

		CheckOut co = CheckOut.instance(CheckOutTest.RULES);
		for (String item : goods.split("")) {
			if (!StringUtils.isEmpty(item)) {
				co.scan(item);
			}
		}
		return co.getTotal();
	}

}
