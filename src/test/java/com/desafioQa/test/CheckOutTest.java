package com.desafioQa.test;

import com.desafioQa.checkout.CheckOut;
import com.desafioQa.checkout.NormalPrice;
import com.desafioQa.checkout.PriceManager;
import com.desafioQa.checkout.SpecialPrice;
import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Testes para validaçao dos comparativos de preços
 * 
 * @author Nickson chagas
 */
public class CheckOutTest extends PriceManager {

	/**
	 * Responsavél por definir preço ao produto. Exemplo de inclusão de um novo
	 * produto com preço normal: RULES.add(new NormalPrice("<Nome_do_Produto>",<Valor_do_Produto>));
	 * --------------------------------------------------------------------
	 * Exemplo de inclusão de um novo produto com preço promocional:
	 * RULES.add(new NormalPrice("<Nome_do_Produto>",<Valor_do_Produto>,<Qantidade_do_Produto>));
	 * --------------------------------------------------------------------
	 */
	
	public static final List<SpecialPrice> RULES = Lists.newArrayList();
	static {
		RULES.add(new NormalPrice("A", 50));
		RULES.add(new NormalPrice("B", 30));
		RULES.add(new NormalPrice("C", 20));
		RULES.add(new NormalPrice("D", 15));
		RULES.add(new SpecialPrice("A", 130, 3));
		RULES.add(new SpecialPrice("B", 45, 2));
	}

	@Test
	public void getTotal() {
		assertEquals(0, price(""));
		assertEquals(50, price("A"));
		assertEquals(80, price("AB"));
		assertEquals(115, price("CDBA"));

		assertEquals(100, price("AA"));
		assertEquals(130, price("AAA"));
		assertEquals(180, price("AAAA"));
		assertEquals(230, price("AAAAA"));
		assertEquals(260, price("AAAAAA"));

		assertEquals(160, price("AAAB"));
		assertEquals(175, price("AAABB"));
		assertEquals(190, price("AAABBD"));
		assertEquals(190, price("DABABA"));
	}

	@Test
	public void incremental() {
		CheckOut testIncremental = CheckOut.instance(RULES);
		assertEquals(0, testIncremental.getTotal());

		testIncremental.scan("A");
		assertEquals(50, testIncremental.getTotal());

		testIncremental.scan("B");
		assertEquals(80, testIncremental.getTotal());

		testIncremental.scan("A");
		assertEquals(130, testIncremental.getTotal());

		testIncremental.scan("A");
		assertEquals(160, testIncremental.getTotal());

		testIncremental.scan("B");
		assertEquals(175, testIncremental.getTotal());
	}

}
